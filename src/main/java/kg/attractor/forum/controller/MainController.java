package kg.attractor.forum.controller;

import kg.attractor.forum.model.Customer;
import kg.attractor.forum.model.CustomerRegisterForm;
import kg.attractor.forum.model.PasswordResetToken;
import kg.attractor.forum.repository.CustomerRepository;
import kg.attractor.forum.repository.ResetRepository;
import kg.attractor.forum.service.CommentService;
import kg.attractor.forum.service.CustomerService;
import kg.attractor.forum.service.PostService;
import kg.attractor.forum.service.PropertiesService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.UUID;

@Controller
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@RequestMapping("/main")
public class MainController {
    private final CustomerService customerService;
    private final CustomerRepository customerRepository;
    private final ResetRepository resetRepo;
    private final PostService postService;
    private final PropertiesService propertiesService;
    private final CommentService commentService;



    @GetMapping("/registration")
    public String pageRegisterCustomer(Model model) {
        if (!model.containsAttribute("dto")) {
            model.addAttribute("dto", new CustomerRegisterForm());
        }
        return "registration";
    }

    @PostMapping("/registration")
    public String registerPage(@Valid CustomerRegisterForm customerRequestDto,
                               BindingResult validationResult,
                               RedirectAttributes attributes) {
        attributes.addFlashAttribute("dto", customerRequestDto);

        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/main/registration";
        }

        customerService.register(customerRequestDto);
        return "redirect:/main/login";
    }

    @GetMapping("/login")
    public String loginPage(@RequestParam(required = false, defaultValue = "false") Boolean error, Model model) {
        model.addAttribute("error", error);
        return "login";
    }

    private static <T> void constructPageable(Page<T> list, int pageSize, Model model, String uri) {
        if (list.hasNext()) {
            model.addAttribute("nextPageLink", constructPageUri(uri, list.nextPageable().getPageNumber(), list.nextPageable().getPageSize()));
        }

        if (list.hasPrevious()) {
            model.addAttribute("prevPageLink", constructPageUri(uri, list.previousPageable().getPageNumber(), list.previousPageable().getPageSize()));
        }

        model.addAttribute("hasNext", list.hasNext());
        model.addAttribute("hasPrev", list.hasPrevious());
        model.addAttribute("items", list.getContent());
        model.addAttribute("defaultPageSize", pageSize);
    }
    private static String constructPageUri(String uri, int page, int size) {
        return String.format("%s?page=%s&size=%s", uri, page, size);
    }
    @GetMapping("/logout")
    public String invalidate(HttpSession session) {
        if (session != null) {
            session.invalidate();
        }

        return "redirect:/main";
    }
    @GetMapping("/forgot-password")
    public String pageForgotPassword() {
        return "forgot";
    }

    @PostMapping("/forgot-password")
    public String submitForgotPasswordPage(@RequestParam("email") String email,
                                           RedirectAttributes attributes) {

        if (!customerRepository.existsByEmail(email)) {
            attributes.addFlashAttribute("errorText", "Entered email does not exist!");
            return "redirect:/main";
        }

        PasswordResetToken pToken = PasswordResetToken.builder()
                .customer(customerRepository.findByEmail(email))
                .token(UUID.randomUUID().toString())
                .build();

        resetRepo.deleteAll();
        resetRepo.save(pToken);

        return "redirect:/main/forgot-success";
    }

    @GetMapping("/forgot-success")
    public String pageResetPassword() {
        return "forgot-success";
    }

    @PostMapping("/reset-password")
    public String submitResetPasswordPage(@RequestParam("token") String token,
                                          @RequestParam("newPassword") String newPassword,
                                          RedirectAttributes attributes) {

        if (!resetRepo.existsByToken(token)) {
            attributes.addFlashAttribute("errorText", "Entered email does not exist!");
            return "redirect:/main/reset-password";
        }
        PasswordResetToken pToken = resetRepo.findByToken(token).get();
        Customer customer = customerRepository.findById(pToken.getCustomer().getId()).get();
        customer.setPassword(new BCryptPasswordEncoder().encode(newPassword));

        customerRepository.save(customer);

        return "redirect:/main/login";
    }
    @GetMapping("/create-post")
    public String createPost(){
        return "createPost";
    }
    @PostMapping("/create-post")
    public String newPost(@RequestParam("text")String text,Authentication authentication,
                          @RequestParam("name")String name){
        postService.createPost(authentication,text,name);
        return "redirect:/main";
    }
    @GetMapping
    public String mainPage(Model model, HttpServletRequest uriBuilder,Pageable pageable) {
        var uri = uriBuilder.getRequestURI();
        var posts = postService.getPosts(pageable);
        constructPageable(posts,propertiesService.getDefaultPageSize(),model,uri);
        return "main";
    }
    @GetMapping("/posts/{id}")
    public String getPost(@PathVariable("id") int id,Authentication authentication,Model model,Pageable pageable,HttpServletRequest uriBuilder){
     var post = postService.getPost(id,authentication);
     var uri = uriBuilder.getRequestURI();
     var comments = commentService.getComments(id,pageable);
     constructPageable(comments,propertiesService.getDefaultPageSize(),model,uri);
     model.addAttribute("post", post);
     return "singlePost";
    }

    @GetMapping("/search/{search}")
    public String search(@PathVariable("search") String search,Model model, Pageable pageable, HttpServletRequest uriBuilder,Authentication authentication){
        var posts = postService.getPostsBySearch(pageable, search);
        var uri = uriBuilder.getRequestURI();
        constructPageable(posts, propertiesService.getDefaultPageSize(), model, uri);
        return "main";
    }
    @GetMapping("/profile")
    public String getProfile(Authentication authentication,Model model){
        var user = customerService.getLoginCustomer(authentication);
        model.addAttribute("customer",user);
        return "profile";
    }
    @PostMapping("/create-comment")
    public String createComment(@RequestParam("text") String text, @RequestParam("id")int id, Authentication authentication){
        commentService.createComment(text, authentication, id);
        return "redirect:/main/posts/"+id;
    }
    }
