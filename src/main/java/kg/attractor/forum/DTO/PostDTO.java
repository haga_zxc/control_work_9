package kg.attractor.forum.DTO;

import kg.attractor.forum.model.Post;
import lombok.*;

import java.sql.Timestamp;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(access = AccessLevel.PACKAGE)
@ToString
public class PostDTO {
    private int id;
    private String text;
    private CustomerDTO customer;
    private Timestamp date;
    private String name;

    public static PostDTO from(Post post){
        return   builder()
                .customer(CustomerDTO.from(post.getCustomer()))
                .date(post.getDate())
                .text(post.getText())
                .id(post.getId())
                .name(post.getName())
                .build();
    }
}
