package kg.attractor.forum.service;

import kg.attractor.forum.DTO.CommentDTO;
import kg.attractor.forum.model.Comment;
import kg.attractor.forum.repository.CommentRepository;
import kg.attractor.forum.repository.CustomerRepository;
import kg.attractor.forum.repository.PostRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
@AllArgsConstructor
public class CommentService {
    private final CommentRepository commentRepository;
    private final CustomerRepository customerRepository;
    private final PostRepository postRepository;

    public Page<CommentDTO> getComments(int id, Pageable pageable){
        return commentRepository.findAllByPostId(id,pageable).map(CommentDTO::from);
    }

    public void createComment(String text, Authentication authentication,int id){
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        var user = customerRepository.findByEmail(userDetails.getUsername());
        var comment = Comment.builder()
                .customer(user)
                .date(new Timestamp(System.currentTimeMillis()))
                .post(postRepository.findById(id).get())
                .text(text)
                .build();
        commentRepository.save(comment);

    }
}
