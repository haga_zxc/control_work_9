package kg.attractor.forum.service;

import kg.attractor.forum.DTO.PostDTO;
import kg.attractor.forum.model.Customer;
import kg.attractor.forum.model.Post;
import kg.attractor.forum.repository.CustomerRepository;
import kg.attractor.forum.repository.PostRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
@AllArgsConstructor
public class PostService {
    private final PostRepository postRepository;
    private final CustomerRepository customerRepository;

    public void createPost(Authentication authentication, String text,String name){
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        var user = customerRepository.findByEmail(userDetails.getUsername());
        var post = Post.builder()
                .text(text)
                .customer(user)
                .date(new Timestamp(System.currentTimeMillis()))
                .name(name)
                .build();
        postRepository.save(post);
    }

    public Page<PostDTO> getPosts(Pageable pageable){
        var posts = postRepository.findAllByOrderByDateAsc(pageable);
       return posts.map(PostDTO::from);
    }
    public PostDTO getPost(int id, Authentication authentication){
        return PostDTO.from(postRepository.findById(id).get());
    }

    public Page<PostDTO> getPostsBySearch(Pageable pageable,String search){
        var posts = postRepository.findProductsByNameAndText(search,pageable);
        return posts.map(PostDTO::from);
    }

}
