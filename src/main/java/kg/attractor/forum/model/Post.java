package kg.attractor.forum.model;


import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Entity
@ToString
@Table(name = "posts")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotBlank
    @Size(min = 1, max = 300)
    @Column(length = 300)
    private String text;
    @NotBlank
    @Size(min = 1, max = 50)
    @Column(length = 50)
    private String name;

    @Column()
    private Timestamp date;

    @ManyToOne(targetEntity = Customer.class)
    @JoinColumn(name = "customers_id")
    private Customer customer;
}
