package kg.attractor.forum.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Entity
@ToString
@Table(name = "comments")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotBlank
    @Size(min = 1, max = 600)
    @Column(length = 600)
    private String text;

    @Column()
    private Timestamp date;

    @ManyToOne(targetEntity = Customer.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "customers_id")
    private Customer customer;

    @OneToOne(targetEntity = Post.class, fetch = FetchType.EAGER)
    @JoinColumn( name = "posts_id")
    private Post post;
}
