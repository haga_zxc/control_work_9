package kg.attractor.forum.repository;

import kg.attractor.forum.model.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PostRepository extends JpaRepository<Post, Integer> {
Page<Post> findAllByOrderByDateAsc (Pageable pageable);
    @Query("SELECT p FROM Post p WHERE (p.text like concat(:name, '%')) or (p.text like concat('%',:name,'%')) or (p.text like concat('%', :name))or (p.name like concat(:name, '%')) or (p.name like concat('%',:name, '%') ) or (p.name like concat('%', :name)) ")
    Page<Post> findProductsByNameAndText(String name, Pageable pageable);
}
