function scrollToBot() {
    window.scrollTo(0, document.body.scrollHeight);
}

function search() {
    let value = document.getElementById("search-input").value;
    console.log(value);
    if(value.length === 0){
        return;
    }else{
        window.location.href = 'http://localhost:4040/main/search/'+value;
    }
};

function getUserId(id) {
    localStorage.setItem("user",id)
}

function formHide() {
    if(localStorage.getItem("user") === null) {
        document.getElementById("form_in_post").hidden = true;
    }else{
        document.getElementById("form_in_post").hidden = false;
    }
}

function onLoad() {
    if(localStorage.getItem("user") === null){
        document.getElementById("btn-register").hidden = false;
        document.getElementById("btn-login").hidden = false;
    }else{
        document.getElementById("btn-register").hidden = true;
        document.getElementById("btn-login").hidden = true;
        let nav = document.getElementsByClassName("navbar")[0];
        let div = document.createElement('div');
        const html = `
<a class="navbar-brand ml-3" href="/main/profile">
    <button type="button" class="btn btn-warning " id="btn-login">Profile</button>
    </a>
    <a class="navbar-brand " href="/main/logout">
    <button type="button" class="btn btn-warning" onclick="localStorage.clear()" id="btn-register">Logout</button>
    </a>
    <a class="navbar-brand " href="/main/create-post">
    <button type="button" class="btn btn-warning" id="btn-register">Create Post</button>
    </a>
    `;
        div.innerHTML += html;
        nav.insertBefore(div, nav.childNodes[2]);

    }
}
