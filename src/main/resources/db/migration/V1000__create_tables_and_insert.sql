use `forum`;

CREATE TABLE `customers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(128) NOT NULL,
  `password` VARCHAR(128) NOT NULL,
  `fullname` VARCHAR(128) NOT NULL,
  `enabled` boolean NOT NULL,
  `role` VARCHAR(16) NOT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `resets` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `token` VARCHAR(128) NOT NULL,
  `customers_id` INT NOT NULL,
  PRIMARY KEY (`id`, `customers_id`),
  CONSTRAINT `fk_resets_customers`
    FOREIGN KEY (`customers_id`) REFERENCES `customers` (`id`)
);


CREATE TABLE `posts` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `text` VARCHAR(300) NOT NULL,
  `date` DATETIME NOT NULL,
  `customers_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_posts_customers` FOREIGN KEY (`customers_id`)REFERENCES `customers` (`id`)
);

CREATE TABLE `comments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `text` VARCHAR(500) NULL,
  `date` DATETIME NULL,
  `posts_id` INT NOT NULL,
  `customers_id` INT NOT NULL,
  PRIMARY KEY (`id`, `posts_id`, `customers_id`),
  CONSTRAINT `fk_comments_posts1`
FOREIGN KEY (`posts_id`)REFERENCES `posts` (`id`),
  CONSTRAINT `fk_comments_customers1`
FOREIGN KEY (`customers_id`)REFERENCES `customers` (`id`)
);

insert into `customers` (email, password, fullname, enabled, role) VALUES
('qwerty@mail.ru','qwerty','Alexey',true,'USER'),
('123@mail.ru','qwerty123','Nadya',true,'USER');

insert into `posts` ( name, text, date, customers_id) VALUES
('Post 1','Post number 1','2020-05-23 11:31:30' ,1),
('Post 2','Post number 2','2020-05-23 11:32:30' ,1),
('Post 3','Post number 3','2020-05-23 11:33:30' ,1),
('Post 4','Post number 4','2020-05-23 11:34:30' ,1),
('Post 5','Post number 5','2020-05-23 11:35:30' ,2),
('Post 6','Post number 6','2020-05-23 11:36:30' ,2),
('Post 7','Post number 7','2020-05-23 11:37:30' ,2),
('Post 8','Post number 8','2020-05-23 11:38:30' ,2);
